A repo for file sharing of MIT's 2.75/6.025 Medical Device Design Syringe Pump lab.
Paste Journal article here once published?

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!-- **Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting) -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
