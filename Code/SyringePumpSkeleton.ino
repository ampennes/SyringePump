/**************************************************************************
 This is an example for our Monochrome OLEDs based on SSD1306 drivers

 Pick one up today in the adafruit shop!
 ------> http://www.adafruit.com/category/63_98

 This example is for a 128x64 pixel display using I2C to communicate
 3 pins are required to interface (two I2C and one reset).

 Adafruit invests time and resources providing this open
 source code, please support Adafruit and open-source
 hardware by purchasing products from Adafruit!

 Written by Limor Fried/Ladyada for Adafruit Industries,
 with contributions from the open source community.
 BSD license, check license.txt for more information
 All text above, and the splash screen below must be
 included in any redistribution.
 **************************************************************************/
// some libraries to make things easy for us
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Wire connections
#define Mode1 3 // 4 is D4 etc.
#define Mode2 2
#define StepPin  5
#define Dir   4
#define Pot A0
#define Button1 6// single press for next, long press for ACK
#define Button2 // maybe not needed

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library. 
// On an arduino UNO or Nano:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Global Variables
int volume=42;
int duration=0;
int hours=0;
int minutes=0;
unsigned long lastPress=0;
int lastButtonState=1;
int MenuState=0;

void setup() {// runs once on startup/reset
  Serial.begin(9600);// lets the Nano talk to your computer
  delay(100);
  Serial.println("init");
  randomSeed(analogRead(A2));
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {// check if the OLED is present
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
//step mode setup (latches at power-up or stby toggle)
pinMode(StepPin, OUTPUT);// output lets us set a pin state
pinMode(Dir, OUTPUT);
pinMode(Mode1, OUTPUT);
pinMode(Mode2, OUTPUT);
pinMode(Pot, INPUT);// input lets us read a pin state
pinMode(Button1, INPUT_PULLUP);// adding the pullup gives the pin a default state
// defaulting to full step
digitalWrite(StepPin,LOW);
digitalWrite(Dir,LOW);
digitalWrite(Mode1,LOW);
digitalWrite(Mode2,LOW);


display.setTextColor(SSD1306_WHITE);        // Draw white text
}

// void testdrawstyles(void) {
//   display.clearDisplay();

//   display.setTextSize(1);             // Normal 1:1 pixel scale
//   display.setTextColor(SSD1306_WHITE);        // Draw white text
//   display.setCursor(0,0);             // Start at top-left corner
//   display.println(F("Hello, world!"));

//   display.display();
//   delay(2000);
// }

void loop(){// runs forever
 Serial.println(analogRead(Pot)); // Spit out pot values to Serial monitor for debugging.  Kill it once it starts annoying you
  SetupMenu();
}

void Purge(){// set direction and drive some distance forward.  Repeat until motor is stalled  UPDATE ME
Serial.println("purge");// included so you know the function is triggered
// the behavior of this function is up to you to write 
// start by setting the direction you want to move  look at the value of Dir
// feel free to use the function Step() below which will move a single step 
// Arduino For or While loops will be useful for you
// If you feel lost just ask!

}


void Prime(){// it's just purge but backwards UPDATE ME
Serial.println("prime");// included so you know the function is triggered
//
}

void Start(){
  digitalWrite(Dir, HIGH);
  Serial.println("dosing");
// steps per mL
// take in total volume and time to dispense then calculate stepping procedure
int stepsPermL=random(200); // UPDATE THIS
int totalSteps= stepsPermL*volume;
minutes=minutes+(hours*60);
unsigned long totalmS=(minutes*60000);
unsigned long stepSpacing= totalmS/totalSteps; // number of miliseconds between steps
Serial.println(stepsPermL);
Serial.println(totalSteps);
Serial.println(totalmS);
Serial.println(stepSpacing);
for(int i=0; i<totalSteps; i++){// this will work but has a number of drawbacks
  digitalWrite(StepPin,HIGH);
  delay(stepSpacing/2);
  digitalWrite(StepPin,LOW);
  delay(stepSpacing/2);//just looping until done with steps
  Serial.println("stepping");
}
Serial.println("done");

}
void Stop(){
  Serial.println("stop");
}





void Step(){// just take 1 step in a given direction
digitalWrite(StepPin, HIGH);
delay(1);
digitalWrite(StepPin, LOW);
delay(1);
}



void SetupMenu(){// simple GUI on the OLED.
  
  

  // Base screen layout
  display.clearDisplay();// wipe the screen
  display.setCursor(0,0);             // Start at top-left corner
  display.setTextSize(1); 
  display.print(F("Dose Volume:"));
  //display.setTextSize(2); 
  char buffer[20];
  sprintf(buffer, " %d mL", volume);
  char buffer2[40];
  sprintf(buffer2, "Over: %d : %d ", hours, minutes);
  display.setCursor(0,8);
  display.print(buffer);
  display.setCursor(0,16);
  display.print(buffer2);
  display.setCursor(0,24);
  display.print("Prime    Purge");
  display.setCursor(0,32);
  display.print("Start    Stop");
  display.display();
  delay(100);
  int Temp=0;
// 
  switch(MenuState){
      case 0:
        display.fillRect(6,7,12,10,SSD1306_BLACK);// cover volume
        display.display();
        Temp=analogRead(Pot);// read the pot value
        volume=map(Temp, 0, 1023, 0, 50);// map pot value from 0-50 mL
        break;

      case 1:
      Serial.println("here");
        display.fillRect(30,15,20,10,SSD1306_BLACK);// cover hours
        display.display();
        Temp=analogRead(Pot);// read the pot value
        hours=map(Temp, 0, 1023, 0, 24);// map pot value from 0-50 mL
        break;
      case 2:
        display.fillRect(60,15,20,10,SSD1306_BLACK);// cover minutes
        display.display();
        Temp=analogRead(Pot);// read the pot value
        minutes=map(Temp, 0, 1023, 0, 60);// map pot value from 0-50 mL
        break;
      case 3:
        display.fillRect(0,23,30,9,SSD1306_BLACK);// cover Prime
        display.display();
        break;
      case 4:
        display.fillRect(50,23,40,9,SSD1306_BLACK);// cover Purge
        display.display();
        break;
      case 5:
        display.fillRect(0,31,30,9,SSD1306_BLACK);// cover Start
        display.display();
        break;
      case 6:
        display.fillRect(50,31,40,9,SSD1306_BLACK);// cover Stop
        display.display();
        break;

    }

  
  // else{// if button is down, check if first press and how long it's been held
  // if (lastButtonState==1){// this is the first press
  //   lastPress=millis();
  //   lastButtonState=0;
  //   Serial.println("LOW");
  // }
  
  // if (millis()-lastPress>500){// button held for half second cycles menu
  // MenuState++;
  // lastButtonState=1;
  // if (MenuState>6){
  //   MenuState=0;
  // }
  // Serial.println(MenuState);

  // }
  if(digitalRead(Button1)==HIGH){
    lastPress=millis();
  }
  while(digitalRead(Button1)==LOW){// 
    if(digitalRead(Button1)==HIGH){
      break;
    }

  }
  if(millis()-lastPress>50 && millis()-lastPress<1000){// short press
    MenuState++;
    if (MenuState>6){
      MenuState=0;  
    }
  }
  if(millis()-lastPress>1000){// long press used to ack functions
    switch(MenuState){
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3://prime
      Prime();
        break;
      case 4://purge
      Purge();
        break;
      case 5://start
      Start();
        break;
      case 6://stop
      Stop();
        break;

    }
}


}

  
